using Go = import "go.capnp";
$Go.package("main");
$Go.import("bench");

@0xaf174bbba13b3dbf;

struct LearnCnp {
       id @0 :Id;
       slot @1 :UInt32;
       round @2 :ProposerRound;
       value @3 :Value;
       epoch @4 :List(UInt64);

       struct Value {
              type @0 :UInt8;
              cr @1 :Request;

              struct Request {
                     type @0 :UInt8;
                     id @1 :Text;
                     seq @2 :UInt32;
                     val @3 :Data;
              }
       }

       struct Id {
              id @0 :Int8;
              epoch @1 :UInt64;
       }

       struct ProposerRound {
              round @0 :UInt32;
              id @1 :Id;
       }
}
