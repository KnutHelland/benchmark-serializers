package main

import (
	"bytes"
	"encoding/gob"
	"runtime"
	"testing"

	"code.google.com/p/goprotobuf/proto"

	capn "github.com/glycerine/go-capnproto"
	"github.com/relab/goxos/clienth"
	"github.com/relab/goxos/grp"
	px "github.com/relab/goxos/paxos"
)


func main() {
	println("NumCPU(): ", runtime.NumCPU(), "\n")

	println("BenchmarkProtobufEncode\t\t\t", testing.Benchmark(BenchmarkProtobufEncode).String())
	println("BenchmarkProtobufDecode\t\t\t", testing.Benchmark(BenchmarkProtobufDecode).String())
	println("BenchmarkCNPEncode\t\t\t", testing.Benchmark(BenchmarkCNPEncode).String())
	println("BenchmarkCNPDecode\t\t\t", testing.Benchmark(BenchmarkCNPDecode).String(), "\n")
	// println("BenchmarkGOBEncodeAndDecode\t\t", testing.Benchmark(BenchmarkGOBEncodeAndDecode).String())
	println("BenchmarkGOBNewStreamEachEncode\t\t", testing.Benchmark(BenchmarkGOBNewStreamEachEncode).String())
	println("BenchmarkGOBOneStream\t\t\t", testing.Benchmark(BenchmarkGOBOneStream).String())
	println("BenchmarkGOBParallelNewStreamEachEncode\t", testing.Benchmark(BenchmarkGOBParallelNewStreamEachEncode).String())
	println("BenchmarkGOBParallelOneStream\t\t", testing.Benchmark(BenchmarkGOBParallelOneStream).String())
}

// This is how we can create a Learn message with our current px.Learn
// in Goxos:
func generateLearn() px.Learn {
	t := clienth.Request_HELLO
	i := "øasdlkfjølkj"
	u := uint32(234234)

	return px.Learn{
		grp.Id{
			grp.PaxosId(10),
			grp.Epoch(321),
		},
		px.SlotId(29),
		px.ProposerRound{
			uint(342),
			grp.Id{
				grp.PaxosId(10),
				grp.Epoch(321),
			},
		},
		px.Value{
			px.App,
			[]*clienth.Request{
				&clienth.Request{
					&t,
					&i,
					&u,
					[]byte{3, 2, 33, 3, 4},
					[]byte{32, 23, 1, 2, 3},
				},
			},
			nil,
		},
		[]grp.Epoch{
			grp.Epoch(321),
			grp.Epoch(322),
			grp.Epoch(323),
		},
	}
}

// Creating a Cap'n Proto learn:
func generateLearnCnp(s *capn.Segment) LearnCnp {
	l := NewRootLearnCnp(s)

	id := NewLearnCnpId(s)
	id.SetId(10)
	id.SetEpoch(321)

	l.SetId(id)
	l.SetSlot(29)

	prnd := NewLearnCnpProposerRound(s)
	prnd.SetRound(342)
	prnd.SetId(id)
	l.SetRound(prnd)

	val := NewLearnCnpValue(s)
	val.SetType(1)
	req := NewLearnCnpValueRequest(s)
	req.SetType(1)
	req.SetId("løadksfjløkj")
	req.SetSeq(32)
	req.SetVal([]byte{21, 32, 42, 12, 34, 45})
	val.SetCr(req)
	l.SetValue(val)

	e := s.NewUInt64List(3)
	e.Set(0, 321)
	e.Set(1, 322)
	e.Set(2, 323)
	l.SetEpoch(e)
	return l
}

// helper functions
func makeInt32Pointer(val int32) *int32 { return &val }
func makeUInt32Pointer(val uint32) *uint32 { return &val }
func makeStringPointer(val string) *string { return &val }
func makeUInt64Pointer(val uint64) *uint64 { return &val }

// Creating a Protobuf learn. Similar to our px.Learn, but all values
// are pointers in these structs.
func generateLearnProto() LearnProto {
	id := &LearnProto_Id{
		Id:    makeInt32Pointer(10),
		Epoch: makeUInt64Pointer(321),
	}

	return LearnProto{
		Id:   id,
		Slot: makeUInt32Pointer(29),
		Round: &LearnProto_ProposerRound{
			Round: makeUInt32Pointer(12),
			Id:    id,
		},
		Value: &LearnProto_Value{
			Type: makeUInt32Pointer(32),
			Cr: &LearnProto_Value_Request{
				Type: makeUInt32Pointer(11),
				Id:   makeStringPointer("ølakdfjslø"),
				Seq:  makeUInt32Pointer(342),
				Val:  []byte{32, 123, 54, 21},
			},
		},
		Epoch: []uint64{1232, 1234, 5424, 6547},
	}
}

func BenchmarkProtobufEncode(b *testing.B) {
	for i := 0; i < b.N; i++ {
		learn := generateLearnProto()
		_, err := proto.Marshal(&learn)
		if err != nil {
			println("encode error", err)
			return
		}
	}
}

func BenchmarkProtobufDecode(b *testing.B) {
	// setup (not benchmarked):
	learn := generateLearnProto()
	data, err := proto.Marshal(&learn)
	if err != nil {
		println("encode error", err)
		return
	}

	// benchmarking starts:
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		l := &LearnProto{}
		err = proto.Unmarshal(data, l)
		if err != nil {
			println("decode error", err)
			return
		}
	}
}


func BenchmarkCNPEncode(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s := capn.NewBuffer(nil)
		generateLearnCnp(s)

		buf := bytes.Buffer{}

		s.WriteTo(&buf)
	}
}

func BenchmarkCNPDecode(b *testing.B) {
	// setup (not benchmarked):
	s := capn.NewBuffer(nil)
	generateLearnCnp(s)
	buf := bytes.Buffer{}
	s.WriteTo(&buf)

	// benchmarking starts:
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s2, err := capn.ReadFromStream(bytes.NewReader(buf.Bytes()), nil)
		if err != nil {
			println("read error", err)
			return
		}
		ReadRootLearnCnp(s2)
	}
}

// Takes a long time to execute because of the heavy pre-benchmark
// work. So I've excluded this test.
func BenchmarkGOBEncodeAndDecode(b *testing.B) {
	bufs := make([]*bytes.Buffer, b.N)
	encs := make([]*gob.Encoder, b.N)
	decs := make([]*gob.Decoder, b.N)

	for i := 0; i < b.N; i++ {
		bufs[i] = &bytes.Buffer{}
		encs[i] = gob.NewEncoder(bufs[i])
		decs[i] = gob.NewDecoder(bufs[i])
		encs[i].Encode(generateLearn())
		var l px.Learn
		decs[i].Decode(&l)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		encs[i].Encode(generateLearn())
		var l px.Learn
		decs[i].Decode(&l)
	}
}

func BenchmarkGOBNewStreamEachEncode(b *testing.B) {
	buffers := make([]*bytes.Buffer, b.N)
	for i := 0; i < b.N; i++ {
		str := ""
		buffers[i] = bytes.NewBufferString(str)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		enc := gob.NewEncoder(buffers[i])
		enc.Encode(generateLearn())
	}
}

func BenchmarkGOBOneStream(b *testing.B) {
	str := ""
	buffer := bytes.NewBufferString(str)
	enc := gob.NewEncoder(buffer)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		enc.Encode(generateLearn())
	}
}

func BenchmarkGOBParallelNewStreamEachEncode(b *testing.B) {
	cores := runtime.NumCPU()

	buffers := make([]*bytes.Buffer, (b.N * cores))
	for i := 0; i < (b.N * cores); i++ {
		str := ""
		buffers[i] = bytes.NewBufferString(str)
	}
	rounds := b.N
	b.ResetTimer()
	stop := make(chan bool)
	for j := 0; j < cores; j++ {
		go func(ja int) {
			for i := 0; i < rounds; i++ {
				enc := gob.NewEncoder(buffers[(i + ja*rounds)])
				enc.Encode(generateLearn())
			}
			stop <- true
		}(j)
	}

	for j := 0; j < cores; j++ {
		<-stop
	}
}

func BenchmarkGOBParallelOneStream(b *testing.B) {
	cores := runtime.NumCPU()

	buffers := make([]*bytes.Buffer, cores)
	for i := 0; i < cores; i++ {
		str := ""
		buffers[i] = bytes.NewBufferString(str)
	}
	rounds := b.N
	b.ResetTimer()
	stop := make(chan bool)
	for j := 0; j < cores; j++ {
		go func(ja int) {
			enc := gob.NewEncoder(buffers[ja])
			for i := 0; i < rounds; i++ {
				enc.Encode(generateLearn())
			}
			stop <- true
		}(j)
	}

	for j := 0; j < cores; j++ {
		<-stop
	}
}
