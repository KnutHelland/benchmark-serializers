package main

// AUTO GENERATED - DO NOT EDIT

import (
	"bufio"
	"bytes"
	"encoding/json"
	C "github.com/glycerine/go-capnproto"
	"io"
	"unsafe"
)

type LearnCnp C.Struct

func NewLearnCnp(s *C.Segment) LearnCnp      { return LearnCnp(s.NewStruct(8, 4)) }
func NewRootLearnCnp(s *C.Segment) LearnCnp  { return LearnCnp(s.NewRootStruct(8, 4)) }
func ReadRootLearnCnp(s *C.Segment) LearnCnp { return LearnCnp(s.Root(0).ToStruct()) }
func (s LearnCnp) Id() LearnCnpId            { return LearnCnpId(C.Struct(s).GetObject(0).ToStruct()) }
func (s LearnCnp) SetId(v LearnCnpId)        { C.Struct(s).SetObject(0, C.Object(v)) }
func (s LearnCnp) Slot() uint32              { return C.Struct(s).Get32(0) }
func (s LearnCnp) SetSlot(v uint32)          { C.Struct(s).Set32(0, v) }
func (s LearnCnp) Round() LearnCnpProposerRound {
	return LearnCnpProposerRound(C.Struct(s).GetObject(1).ToStruct())
}
func (s LearnCnp) SetRound(v LearnCnpProposerRound) { C.Struct(s).SetObject(1, C.Object(v)) }
func (s LearnCnp) Value() LearnCnpValue             { return LearnCnpValue(C.Struct(s).GetObject(2).ToStruct()) }
func (s LearnCnp) SetValue(v LearnCnpValue)         { C.Struct(s).SetObject(2, C.Object(v)) }
func (s LearnCnp) Epoch() C.UInt64List              { return C.UInt64List(C.Struct(s).GetObject(3)) }
func (s LearnCnp) SetEpoch(v C.UInt64List)          { C.Struct(s).SetObject(3, C.Object(v)) }
func (s LearnCnp) WriteJSON(w io.Writer) error {
	b := bufio.NewWriter(w)
	var err error
	var buf []byte
	_ = buf
	err = b.WriteByte('{')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"id\":")
	if err != nil {
		return err
	}
	{
		s := s.Id()
		err = s.WriteJSON(b)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"slot\":")
	if err != nil {
		return err
	}
	{
		s := s.Slot()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"round\":")
	if err != nil {
		return err
	}
	{
		s := s.Round()
		err = s.WriteJSON(b)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"value\":")
	if err != nil {
		return err
	}
	{
		s := s.Value()
		err = s.WriteJSON(b)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"epoch\":")
	if err != nil {
		return err
	}
	{
		s := s.Epoch()
		{
			err = b.WriteByte('[')
			if err != nil {
				return err
			}
			for i, s := range s.ToArray() {
				if i != 0 {
					_, err = b.WriteString(", ")
				}
				if err != nil {
					return err
				}
				buf, err = json.Marshal(s)
				if err != nil {
					return err
				}
				_, err = b.Write(buf)
				if err != nil {
					return err
				}
			}
			err = b.WriteByte(']')
		}
		if err != nil {
			return err
		}
	}
	err = b.WriteByte('}')
	if err != nil {
		return err
	}
	err = b.Flush()
	return err
}
func (s LearnCnp) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	err := s.WriteJSON(&b)
	return b.Bytes(), err
}

type LearnCnp_List C.PointerList

func NewLearnCnpList(s *C.Segment, sz int) LearnCnp_List {
	return LearnCnp_List(s.NewCompositeList(8, 4, sz))
}
func (s LearnCnp_List) Len() int          { return C.PointerList(s).Len() }
func (s LearnCnp_List) At(i int) LearnCnp { return LearnCnp(C.PointerList(s).At(i).ToStruct()) }
func (s LearnCnp_List) ToArray() []LearnCnp {
	return *(*[]LearnCnp)(unsafe.Pointer(C.PointerList(s).ToArray()))
}

type LearnCnpValue C.Struct

func NewLearnCnpValue(s *C.Segment) LearnCnpValue      { return LearnCnpValue(s.NewStruct(8, 1)) }
func NewRootLearnCnpValue(s *C.Segment) LearnCnpValue  { return LearnCnpValue(s.NewRootStruct(8, 1)) }
func ReadRootLearnCnpValue(s *C.Segment) LearnCnpValue { return LearnCnpValue(s.Root(0).ToStruct()) }
func (s LearnCnpValue) Type() uint8                    { return C.Struct(s).Get8(0) }
func (s LearnCnpValue) SetType(v uint8)                { C.Struct(s).Set8(0, v) }
func (s LearnCnpValue) Cr() LearnCnpValueRequest {
	return LearnCnpValueRequest(C.Struct(s).GetObject(0).ToStruct())
}
func (s LearnCnpValue) SetCr(v LearnCnpValueRequest) { C.Struct(s).SetObject(0, C.Object(v)) }
func (s LearnCnpValue) WriteJSON(w io.Writer) error {
	b := bufio.NewWriter(w)
	var err error
	var buf []byte
	_ = buf
	err = b.WriteByte('{')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"type\":")
	if err != nil {
		return err
	}
	{
		s := s.Type()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"cr\":")
	if err != nil {
		return err
	}
	{
		s := s.Cr()
		err = s.WriteJSON(b)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte('}')
	if err != nil {
		return err
	}
	err = b.Flush()
	return err
}
func (s LearnCnpValue) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	err := s.WriteJSON(&b)
	return b.Bytes(), err
}

type LearnCnpValue_List C.PointerList

func NewLearnCnpValueList(s *C.Segment, sz int) LearnCnpValue_List {
	return LearnCnpValue_List(s.NewCompositeList(8, 1, sz))
}
func (s LearnCnpValue_List) Len() int { return C.PointerList(s).Len() }
func (s LearnCnpValue_List) At(i int) LearnCnpValue {
	return LearnCnpValue(C.PointerList(s).At(i).ToStruct())
}
func (s LearnCnpValue_List) ToArray() []LearnCnpValue {
	return *(*[]LearnCnpValue)(unsafe.Pointer(C.PointerList(s).ToArray()))
}

type LearnCnpValueRequest C.Struct

func NewLearnCnpValueRequest(s *C.Segment) LearnCnpValueRequest {
	return LearnCnpValueRequest(s.NewStruct(8, 2))
}
func NewRootLearnCnpValueRequest(s *C.Segment) LearnCnpValueRequest {
	return LearnCnpValueRequest(s.NewRootStruct(8, 2))
}
func ReadRootLearnCnpValueRequest(s *C.Segment) LearnCnpValueRequest {
	return LearnCnpValueRequest(s.Root(0).ToStruct())
}
func (s LearnCnpValueRequest) Type() uint8     { return C.Struct(s).Get8(0) }
func (s LearnCnpValueRequest) SetType(v uint8) { C.Struct(s).Set8(0, v) }
func (s LearnCnpValueRequest) Id() string      { return C.Struct(s).GetObject(0).ToText() }
func (s LearnCnpValueRequest) SetId(v string)  { C.Struct(s).SetObject(0, s.Segment.NewText(v)) }
func (s LearnCnpValueRequest) Seq() uint32     { return C.Struct(s).Get32(4) }
func (s LearnCnpValueRequest) SetSeq(v uint32) { C.Struct(s).Set32(4, v) }
func (s LearnCnpValueRequest) Val() []byte     { return C.Struct(s).GetObject(1).ToData() }
func (s LearnCnpValueRequest) SetVal(v []byte) { C.Struct(s).SetObject(1, s.Segment.NewData(v)) }
func (s LearnCnpValueRequest) WriteJSON(w io.Writer) error {
	b := bufio.NewWriter(w)
	var err error
	var buf []byte
	_ = buf
	err = b.WriteByte('{')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"type\":")
	if err != nil {
		return err
	}
	{
		s := s.Type()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"id\":")
	if err != nil {
		return err
	}
	{
		s := s.Id()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"seq\":")
	if err != nil {
		return err
	}
	{
		s := s.Seq()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"val\":")
	if err != nil {
		return err
	}
	{
		s := s.Val()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte('}')
	if err != nil {
		return err
	}
	err = b.Flush()
	return err
}
func (s LearnCnpValueRequest) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	err := s.WriteJSON(&b)
	return b.Bytes(), err
}

type LearnCnpValueRequest_List C.PointerList

func NewLearnCnpValueRequestList(s *C.Segment, sz int) LearnCnpValueRequest_List {
	return LearnCnpValueRequest_List(s.NewCompositeList(8, 2, sz))
}
func (s LearnCnpValueRequest_List) Len() int { return C.PointerList(s).Len() }
func (s LearnCnpValueRequest_List) At(i int) LearnCnpValueRequest {
	return LearnCnpValueRequest(C.PointerList(s).At(i).ToStruct())
}
func (s LearnCnpValueRequest_List) ToArray() []LearnCnpValueRequest {
	return *(*[]LearnCnpValueRequest)(unsafe.Pointer(C.PointerList(s).ToArray()))
}

type LearnCnpId C.Struct

func NewLearnCnpId(s *C.Segment) LearnCnpId      { return LearnCnpId(s.NewStruct(16, 0)) }
func NewRootLearnCnpId(s *C.Segment) LearnCnpId  { return LearnCnpId(s.NewRootStruct(16, 0)) }
func ReadRootLearnCnpId(s *C.Segment) LearnCnpId { return LearnCnpId(s.Root(0).ToStruct()) }
func (s LearnCnpId) Id() int8                    { return int8(C.Struct(s).Get8(0)) }
func (s LearnCnpId) SetId(v int8)                { C.Struct(s).Set8(0, uint8(v)) }
func (s LearnCnpId) Epoch() uint64               { return C.Struct(s).Get64(8) }
func (s LearnCnpId) SetEpoch(v uint64)           { C.Struct(s).Set64(8, v) }
func (s LearnCnpId) WriteJSON(w io.Writer) error {
	b := bufio.NewWriter(w)
	var err error
	var buf []byte
	_ = buf
	err = b.WriteByte('{')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"id\":")
	if err != nil {
		return err
	}
	{
		s := s.Id()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"epoch\":")
	if err != nil {
		return err
	}
	{
		s := s.Epoch()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte('}')
	if err != nil {
		return err
	}
	err = b.Flush()
	return err
}
func (s LearnCnpId) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	err := s.WriteJSON(&b)
	return b.Bytes(), err
}

type LearnCnpId_List C.PointerList

func NewLearnCnpIdList(s *C.Segment, sz int) LearnCnpId_List {
	return LearnCnpId_List(s.NewCompositeList(16, 0, sz))
}
func (s LearnCnpId_List) Len() int            { return C.PointerList(s).Len() }
func (s LearnCnpId_List) At(i int) LearnCnpId { return LearnCnpId(C.PointerList(s).At(i).ToStruct()) }
func (s LearnCnpId_List) ToArray() []LearnCnpId {
	return *(*[]LearnCnpId)(unsafe.Pointer(C.PointerList(s).ToArray()))
}

type LearnCnpProposerRound C.Struct

func NewLearnCnpProposerRound(s *C.Segment) LearnCnpProposerRound {
	return LearnCnpProposerRound(s.NewStruct(8, 1))
}
func NewRootLearnCnpProposerRound(s *C.Segment) LearnCnpProposerRound {
	return LearnCnpProposerRound(s.NewRootStruct(8, 1))
}
func ReadRootLearnCnpProposerRound(s *C.Segment) LearnCnpProposerRound {
	return LearnCnpProposerRound(s.Root(0).ToStruct())
}
func (s LearnCnpProposerRound) Round() uint32      { return C.Struct(s).Get32(0) }
func (s LearnCnpProposerRound) SetRound(v uint32)  { C.Struct(s).Set32(0, v) }
func (s LearnCnpProposerRound) Id() LearnCnpId     { return LearnCnpId(C.Struct(s).GetObject(0).ToStruct()) }
func (s LearnCnpProposerRound) SetId(v LearnCnpId) { C.Struct(s).SetObject(0, C.Object(v)) }
func (s LearnCnpProposerRound) WriteJSON(w io.Writer) error {
	b := bufio.NewWriter(w)
	var err error
	var buf []byte
	_ = buf
	err = b.WriteByte('{')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"round\":")
	if err != nil {
		return err
	}
	{
		s := s.Round()
		buf, err = json.Marshal(s)
		if err != nil {
			return err
		}
		_, err = b.Write(buf)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte(',')
	if err != nil {
		return err
	}
	_, err = b.WriteString("\"id\":")
	if err != nil {
		return err
	}
	{
		s := s.Id()
		err = s.WriteJSON(b)
		if err != nil {
			return err
		}
	}
	err = b.WriteByte('}')
	if err != nil {
		return err
	}
	err = b.Flush()
	return err
}
func (s LearnCnpProposerRound) MarshalJSON() ([]byte, error) {
	b := bytes.Buffer{}
	err := s.WriteJSON(&b)
	return b.Bytes(), err
}

type LearnCnpProposerRound_List C.PointerList

func NewLearnCnpProposerRoundList(s *C.Segment, sz int) LearnCnpProposerRound_List {
	return LearnCnpProposerRound_List(s.NewCompositeList(8, 1, sz))
}
func (s LearnCnpProposerRound_List) Len() int { return C.PointerList(s).Len() }
func (s LearnCnpProposerRound_List) At(i int) LearnCnpProposerRound {
	return LearnCnpProposerRound(C.PointerList(s).At(i).ToStruct())
}
func (s LearnCnpProposerRound_List) ToArray() []LearnCnpProposerRound {
	return *(*[]LearnCnpProposerRound)(unsafe.Pointer(C.PointerList(s).ToArray()))
}
