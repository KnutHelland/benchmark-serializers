# Install and run

You need to have configured your GOPATH.

```bash
go get bitbucket.org/KnutHelland/benchmark-serializers
cd $GOPATH/src/bitbucket.org/KnutHelland/benchmark-serializers
# Get other dependencies. (need access to goxos on GitHub):
go get ./...
go build
./benchmark-serializers
```

Example output:

```
NumCPU():  4 

BenchmarkProtobufEncode					   500000	      3955 ns/op
BenchmarkProtobufDecode					   500000	      3761 ns/op
BenchmarkCNPEncode						   200000	      8624 ns/op
BenchmarkCNPDecode						  1000000	      2782 ns/op 

BenchmarkGOBNewStreamEachEncode			    50000	     39465 ns/op
BenchmarkGOBOneStream					   500000	      3594 ns/op
BenchmarkGOBParallelNewStreamEachEncode	    10000	    147223 ns/op
BenchmarkGOBParallelOneStream			   200000	     14343 ns/op
```
